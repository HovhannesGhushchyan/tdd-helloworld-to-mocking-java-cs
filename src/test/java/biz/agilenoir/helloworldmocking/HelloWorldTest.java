package biz.agilenoir.helloworldmocking;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ObjectInputStream.GetField;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/*
 * References on the Java Time APIs: 
 * https://www.baeldung.com/java-clock
 * https://docs.oracle.com/javase/8/docs/api/java/time/Clock.html
 * https://www.javatpoint.com/java-get-current-date
 */

class HelloWorldTest {

	@Test
	public void sayHello()
	{
		HelloWorld hello = new HelloWorld();
		LocalDateTime mockTime = LocalDateTime.now();
		hello.setLocalDateTime(mockTime);
		
		assertEquals("Hello World! The time is: " + outputTime(mockTime), hello.sayHello());
	}
	
	// see example for printing time at: https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html
	private String outputTime(LocalDateTime dateTime) {
		return dateTime.format(DateTimeFormatter.ISO_LOCAL_TIME);
	}
}