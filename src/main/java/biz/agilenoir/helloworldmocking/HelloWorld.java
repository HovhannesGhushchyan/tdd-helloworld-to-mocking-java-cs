package biz.agilenoir.helloworldmocking;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class HelloWorld {

	private LocalDateTime dateTimeMock;
	
	void setLocalDateTime(LocalDateTime mock) {
		dateTimeMock = mock;
	}
	
	private LocalDateTime getLocalDateTime()	{
		return (dateTimeMock != null) ? dateTimeMock : LocalDateTime.now();
	}
	
	public String sayHello() {
		return "Hello World! The time is: " + computeTime();
	}
	
	private String computeTime() {
		return getLocalDateTime().format(DateTimeFormatter.ISO_LOCAL_TIME);
	}
}
